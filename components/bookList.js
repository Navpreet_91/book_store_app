import React, { Component } from "react";
import {
  StyleSheet,
  FlatList,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Button,
  ImageBackground,
} from "react-native";
import Modal from "react-native-modal";
import { Icon } from "react-native-elements";

export default class BookList extends Component {
  constructor(props) {
    super(props);
    //sample array data of books
    (this.array = [
      {
        id: "1",
        name: "First Book",
        price: "27.99",
        category: "Food",
        description: "",
      },
      {
        id: "2",
        name: "Second Book",
        price: "50",
        category: "Sports",
        description: "",
      },
      {
        id: "3",
        name: "Third Book",
        price: "35",
        category: "Education",
        description: "",
      },
    ]),
      (this.state = {
        arrayHolder: [],
        modalVisible: false,
        modal01: false,
        textInput_Name: "",
        textInput_Price: "",
        textInput_Category: "",
        textInput_Des: "",
      });
  }

  componentDidMount() {
    this.setState({ arrayHolder: [...this.array] });
  }

  //delete function to delete a book.
  deleteItem = (item) => {
    // this.array.filter((book) => book.id !== item.id);
    // this.setState({ arrayHolder: [...this.array] });
    this.array.pop({
      name: this.state.textInput_Name,
      price: this.state.textInput_Price,
      category: this.state.textInput_Category,
      description: this.state.textInput_Des,
    });

    this.setState({ arrayHolder: [...this.array] });
  };

  //function to add new book to a list
  joinData = () => {
    this.array.push({
      name: this.state.textInput_Name,
      price: this.state.textInput_Price,
      category: this.state.textInput_Category,
      description: this.state.textInput_Des,
    });

    this.setState({ arrayHolder: [...this.array] });
  };

  //alert to show when new book is added
  openAlert = () => {
    alert("Book added successfully.");
  };

  //alert to show when a book is deleted
  delAlert = () => {
    alert("Book deleted successfully.");
  };

  //setting state for modal visible when add new book button clicked
  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  };

  render() {
    const { modalVisible } = this.state;
    const image = {
      uri:
        "https://www.backdropexpress.com/v/vspfiles/photos/KID-JK-013-2.jpg?v-cache=1566227224",
    };
    return (
      <View style={styles.MainContainer}>
        <ImageBackground source={image} style={{ flex: 1 }}>
          <TouchableOpacity
            onPress={() => {
              this.setModalVisible(true);
            }}
            activeOpacity={0.7}
            style={styles.button}
          >
            <Text style={styles.buttonText}>Add a new book </Text>
          </TouchableOpacity>

          {/* Opens Modal to add a new  book */}

          <Modal
            isVisible={modalVisible}
            // animationIn="slideInUp"
            // animationOut="slideOutDown"
            // avoidKeyboard={true}
            transparent={true}
          >
            <View style={{ backgroundColor: "white" }}>
              <Icon
                name="cancel"
                type="MaterialIcons"
                size={30}
                onPress={() => {
                  this.setModalVisible(false);
                }}
              />
              <Text style={styles.item}>Add a new book details here.</Text>
              <TextInput
                placeholder="Enter Book Name"
                onChangeText={(data) => this.setState({ textInput_Name: data })}
                style={styles.textInputStyle}
                underlineColorAndroid="transparent"
              />

              <TextInput
                placeholder="Enter price"
                onChangeText={(data) =>
                  this.setState({ textInput_Price: data })
                }
                style={styles.textInputStyle}
                underlineColorAndroid="transparent"
              />
              <TextInput
                placeholder="Enter Category"
                onChangeText={(data) =>
                  this.setState({ textInput_Category: data })
                }
                style={styles.textInputStyle}
                underlineColorAndroid="transparent"
              />
              <TextInput
                placeholder="Enter Description"
                onChangeText={(data) => this.setState({ textInput_Des: data })}
                style={styles.textInputStyle}
                underlineColorAndroid="transparent"
              />
              <View style={styles.Modalbutton}>
                <Button
                  title="Add a book"
                  color="black"
                  onPress={() => {
                    this.joinData();
                    this.openAlert();
                    this.setModalVisible(false);
                  }}
                />
              </View>
            </View>
          </Modal>

          {/* Opens Modal on click of each book */}
          <Modal transparent={true} isVisible={this.state.modal01}>
            <View
              style={{
                backgroundColor: "white",
              }}
            >
              <Icon
                name="cancel"
                type="MaterialIcons"
                size={40}
                onPress={() => this.setState({ modal01: false })}
              />
              <Text style={styles.item}>Update the book details here.</Text>
              <TextInput
                placeholder={this.state.textInput_Name}
                onChangeText={(data) => this.setState({ textInput_Name: data })}
                style={styles.textInputStyle}
                underlineColorAndroid="transparent"
              />
              <TextInput
                placeholder={this.state.textInput_Price}
                onChangeText={(data) =>
                  this.setState({ textInput_Price: data })
                }
                style={styles.textInputStyle}
                underlineColorAndroid="transparent"
              />
              <TextInput
                placeholder={this.state.textInput_Category}
                onChangeText={(data) =>
                  this.setState({ textInput_Category: data })
                }
                style={styles.textInputStyle}
                underlineColorAndroid="transparent"
              />
              <TextInput
                placeholder={this.state.textInput_Des}
                onChangeText={(data) => this.setState({ textInput_Des: data })}
                style={styles.textInputStyle}
                underlineColorAndroid="transparent"
              />
              <View style={styles.Modalbutton}>
                <Button title="Update Deatils" color="black" />
              </View>
            </View>
          </Modal>

          <FlatList
            data={this.state.arrayHolder}
            width="100%"
            extraData={this.state.array}
            keyExtractor={(item) => item.id}
            // ItemSeparatorComponent={this.FlatListItemSeparator}
            renderItem={({ item, index }) => (
              <>
                <Text
                  style={styles.item}
                  key={item.id}
                  onPress={() => this.setState({ modal01: true })}
                >
                  BOOK NAME {"  "}: {item.name}
                  {"\n"}BOOK PRICE {"  "}: ${item.price}
                  {"\n"}CATEGORY {"     "}: {item.category}
                  {"\n"}
                  {item.description ? item.description : ""}
                  <Icon
                    name="delete"
                    type="MaterialIcons"
                    size={30}
                    color="#B20000"
                    containerStyle={{ paddingLeft: 250 }}
                    onPress={() => {
                      this.deleteItem(item);
                      this.delAlert();
                    }}
                  />
                </Text>
              </>
            )}
          />
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    marginTop: 10,
  },
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: "#56AB2F",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    fontSize: 16,
    fontWeight: "bold",
    borderRadius: 20,
  },

  textInputStyle: {
    textAlign: "center",
    height: 40,
    width: "90%",
    borderWidth: 1,
    borderColor: "#4CAF50",
    borderRadius: 7,
    marginTop: 12,
  },

  button: {
    width: "50%",
    height: 40,
    padding: 10,
    backgroundColor: "black",
    borderRadius: 15,
    marginTop: 10,
    alignSelf: "center",
    marginBottom: 20,
  },

  buttonText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 16,
  },
  textInputStyle: {
    textAlign: "center",
    height: 40,
    width: "90%",
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 7,
    marginTop: 12,
    marginLeft: "auto",
    marginRight: "auto",
  },
  Modalbutton: {
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },
});
