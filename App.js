import React from "react";
import { StyleSheet, Text, View ,ImageBackground} from "react-native";
import BookListData from "./components/bookList";

export default function App() {
  const image = { uri: "https://reactjs.org/logo-og.png" };
  return (
    <>
      <View style={styles.container}>

        <Text style={styles.text}>BOOK LIST</Text>
      </View>
      <BookListData />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#56AB2F",
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
  },
  text: {
    fontSize: 30,
    fontWeight: "bold",
    marginTop: 60,
    marginBottom: 20,
    color: "black",
    fontStyle:'italic'
    
  },
});
